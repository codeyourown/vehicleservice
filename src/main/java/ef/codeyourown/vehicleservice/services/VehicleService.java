package ef.codeyourown.vehicleservice.services;

import ef.codeyourown.vehicleservice.models.Vehicle;

import java.util.List;

public interface VehicleService {
    List<Vehicle> getVehicles();
}
