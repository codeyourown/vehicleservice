package ef.codeyourown.vehicleservice.controller;

import ef.codeyourown.vehicleservice.services.VehicleService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class VehicleController {
    private VehicleService vehicleservice;

    public VehicleController(VehicleService vehicleservice) {
        this.vehicleservice = vehicleservice;
    }

    //Endpoints underneath
}
