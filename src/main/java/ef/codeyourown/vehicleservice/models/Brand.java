package ef.codeyourown.vehicleservice.models;

public enum Brand {
    Mercedes,
    VW,
    Skoda,
    Ford
}
