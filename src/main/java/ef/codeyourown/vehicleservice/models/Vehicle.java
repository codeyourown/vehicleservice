package ef.codeyourown.vehicleservice.models;

public class Vehicle {
    private long id;
    private String license;
    private Brand brand;
    private int seats;

    public Vehicle(String license, Brand brand, int seats) {
        this.license = license;
        this.brand = brand;
        this.seats = seats;
    }

    public long getId() {
        return id;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
}
